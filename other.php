<?php

function foo($one = null, $two = 0, $three = "String")
{
    $f = "The first value";
    $second = "The second value";
    echo 'Hello, world!';
    $t = "The third value";
    $fourth = "The fourth value";
    if (true) {
        $x = array(
            0 => "zero",
            123 => "one two three",
            25 => "two five"
        );
    } else {
        return 0;
    }

    return 1;
}

?>
<?php

/**
 * This is a sample function to illustrate additional
 * PHP formatter options.
 *
 * @param        $one The first parameter
 * @param int    $two The second parameter
 * @param string $three The third parameter
 *
 * @author J.S.
 * @license GPL
 */
function foo($one, $two = 0, $three = "String")
{
}
